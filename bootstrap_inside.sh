#!/bin/bash

set -eux

_USERNAME=user

# add user

useradd --create-home --shell /usr/bin/zsh "${_USERNAME}"

# add user to groups: wheel, wireshark

usermod -a -G wireshark "${_USERNAME}"
usermod -a -G wheel "${_USERNAME}"

# allow group wheel to use sudo

sed -i 's/.*%wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

# set clock

ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc

# generate and set locales

cat >> /etc/locale.gen <<EOL
en_US.UTF-8 UTF-8
de_DE.UTF-8 UTF-8
EOL

locale-gen

cat > /etc/locale.conf <<EOL
LANG=en_US.UTF-8
LC_CTYPE=en_US.UTF-8
LC_NUMERIC=en_US.UTF-8
LC_TIME=de_DE.UTF-8
LC_COLLATE=C
LC_MONETARY=de_DE.UTF-8
LC_MESSAGES=en_US.UTF-8
LC_PAPER=de_DE.UTF-8
LC_NAME=en_US.UTF-8
LC_ADDRESS=en_US.UTF-8
LC_TELEPHONE=en_US.UTF-8
LC_MEASUREMENT=de_DE.UTF-8
LC_IDENTIFICATION=en_US.UTF-8
EOL
chmod 0644 /etc/locale.conf

## set keyboard layout

cat > /etc/vconsole.conf <<EOL
KEYMAP=de-latin1
EOL

# initramfs

# sed -i 's/^FILES=.*/FILES="\/crypto_keyfile.bin"/' /etc/mkinitcpio.conf
sed -i 's/^HOOKS=.*/HOOKS="base udev autodetect modconf block keyboard keymap encrypt lvm2 fsck filesystems"/' /etc/mkinitcpio.conf
# dd if=/dev/urandom of=/crypto_keyfile.bin bs=1 count=2048
#chmod 0 /crypto_keyfile.bin
# NOTE: the crypto_keyfile has to be added manually, run 'mkinitcpio -p linux-hardened'

# set password

echo "change password for '${_USERNAME}'"
passwd "${_USERNAME}"
