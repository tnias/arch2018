#!/bin/bash

set -eu

# this shall be executed as root
# also, at this point your root fs should be mounted in /mnt

PACKAGES="$(cat packagelist.txt)"

# Next line is supposed to split words
pacstrap /mnt base base-devel $PACKAGES
genfstab -U /mnt >> /mnt/etc/fstab
