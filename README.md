
## Install

Execute `bootstrap_outside.sh` from your install system.  Then `arch-chroot /mnt` into
the new system and execute `bootstrap_inside.sh`, make sure you can reach it from within
the chroot environment.

After that the basesystem is ready and ansible scripts can be used to finish the setup.


## Warning

- German keyboard layout and locales hardcoded 
- do this at your own risk
- this is aimed at _my_ desktop systems
- hostname is not set
- no bootloader gets installed (do this yourself, if you need one)

